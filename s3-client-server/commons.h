#pragma once

#include <s3client.h>
#include <s3server.h>
#include <boost/asio.hpp>
#include <vector>
#include <chrono>

namespace asio = boost::asio;
namespace ip = asio::ip;
using tcp = ip::tcp;

tcp::endpoint localhost(ip::address::from_string("127.0.0.1"), 8000);

class ClientWrapper {
public:
    ClientWrapper(tcp::endpoint endpoint) : client_(io_service_, endpoint) {
        io_service_.run();
    }

    int32_t Sum(const std::vector<int>& query) {
        return client_.Sum(query);
    }
private:
    asio::io_service io_service_;
    S3Client client_;
};

class ServerWrapper {
public:
    ServerWrapper(tcp::endpoint endpoint, int max_active_conn)
        : server_(io_service_, endpoint, max_active_conn) {}

    void Start() {
        server_.Start();
    }

    void Shutdown() {
        server_.Shutdown();
    }

    int32_t ProcessedQueryCount() {
        return server_.ProcessedQueryCount();
    }

private:
    asio::io_service io_service_;
    S3Server server_;
};
